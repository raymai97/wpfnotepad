﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace WpfNotepad
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {

		public static string UILanguageCode
		{
			get
			{
				var dicts = Current.Resources.MergedDictionaries;
				var code = dicts[1].Source.ToString();
				code = System.IO.Path.GetFileNameWithoutExtension(code);
				code = System.IO.Path.GetExtension(code).Substring(1);
				return code;
			}
			set
			{
				var dicts = Current.Resources.MergedDictionaries;
				dicts[1].Source = new Uri($"Res/MUI.{value}.xaml", UriKind.Relative);
			}
		}


    }
}
