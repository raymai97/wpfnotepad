﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace WpfNotepad
{
	public class PaddingSetter
	{
		public static readonly DependencyProperty PaddingProperty =
			DependencyProperty.RegisterAttached(
				"Padding",
				typeof(Thickness),
				typeof(PaddingSetter),
				new UIPropertyMetadata(
					new Thickness(),
					OnPaddingChanged
				)
			);

		public static Thickness GetPadding(DependencyObject obj)
		{
			return (Thickness)obj.GetValue(PaddingProperty);
		}

		public static void SetPadding(DependencyObject obj, Thickness value)
		{
			obj.SetValue(PaddingProperty, value);
		}

		public static void OnPaddingChanged(object sender, DependencyPropertyChangedEventArgs e)
		{
			((Control)sender).Loaded += new RoutedEventHandler(host_Loaded);
		}

		static void host_Loaded(object sender, RoutedEventArgs e)
		{
			var panel = sender as ItemsControl;
			foreach (var child in panel.Items)
			{
				var mi = child as Control;
				if (mi != null) mi.Padding = GetPadding(panel);
			}
		}


	}
}
