﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfFontDialog
{
    /// <summary>
    /// Interaction logic for FontDialog.xaml
    /// </summary>
    public partial class FontDialog : Window
    {
        private FontInfo selectedFont;

        public FontInfo Font
        {
            get
            {
                return selectedFont;
            }
            set
            {
                selectedFont = value;
            }
        }

        public FontDialog(bool previewFontInFontList=true)
        {
            InitializeComponent();
			fontChooser.PreviewFontInFontList = previewFontInFontList;
        }

        private void SyncFontName()
        {
            string fontFamilyName = selectedFont.Family.Source;
            bool foundMatch=false;
            int idx = 0;
            foreach (object item in (IEnumerable)fontChooser.lstFamily.Items)
            {
                if (fontFamilyName == item.ToString())
                {
                    foundMatch = true;
                    break;
                }
                idx++;
            }
            if(!foundMatch)
            {
                idx = 0;
            }
            fontChooser.lstFamily.SelectedIndex = idx;
            fontChooser.lstFamily.ScrollIntoView(fontChooser.lstFamily.Items[idx]);
        }

        private void SyncFontSize()
        {
            double fontSize = selectedFont.Size;
            foreach (ListBoxItem item in (IEnumerable)fontChooser.lstFontSizes.Items)
            {
                if (double.Parse(item.Content.ToString()) != fontSize)
                {
                    continue;
                }
                item.IsSelected = true;
                break;
            }
        }

        private void SyncFontTypeface()
        {
            string fontTypeFaceSb = FontInfo.TypefaceToString(selectedFont.Typeface);
            int idx = 0;
            foreach (object item in (IEnumerable)fontChooser.lstTypefaces.Items)
            {
                if (fontTypeFaceSb == FontInfo.TypefaceToString(item as FamilyTypeface))
                {
                    break;
                }
                idx++;
            }
            fontChooser.lstTypefaces.SelectedIndex = idx;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            SyncFontName();
            SyncFontSize();
            SyncFontTypeface();
		}

		private void btnOk_Click(object sender, RoutedEventArgs e)
		{
			Font = fontChooser.SelectedFont;
			DialogResult = new bool?(true);
		}

	}
}
