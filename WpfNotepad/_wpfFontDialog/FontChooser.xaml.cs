﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfFontDialog
{
    /// <summary>
    /// Interaction logic for FontChooser.xaml
    /// </summary>
    public partial class FontChooser : UserControl
    {
        public FontInfo SelectedFont
        {
            get
            {
                return new FontInfo(this.txtSampleText.FontFamily, this.txtSampleText.FontSize, this.txtSampleText.FontStyle, this.txtSampleText.FontStretch, this.txtSampleText.FontWeight);
            }
        }

        public bool PreviewFontInFontList
        {
            get { return (bool)GetValue(PreviewFontInFontListProperty); }
            set { SetValue(PreviewFontInFontListProperty, value); }
        }

        public static readonly DependencyProperty PreviewFontInFontListProperty =
            DependencyProperty.Register(
				"PreviewFontInFontList",
				typeof(bool),
				typeof(FontChooser),
				new PropertyMetadata(
					true, PreviewFontInFontListPropertyCallback
				)
			);

        public FontChooser()
        {
            InitializeComponent();
			// Auto scroll to selected item
			lstFamily.SelectionChanged += (s, e) =>
				(s as ListBox).ScrollIntoView((s as ListBox).SelectedItem);
			lstTypefaces.SelectionChanged += (s, e) =>
				(s as ListBox).ScrollIntoView((s as ListBox).SelectedItem);
			lstFontSizes.SelectionChanged += (s, e) =>
				(s as ListBox).ScrollIntoView((s as ListBox).SelectedItem);
		}

		private static void PreviewFontInFontListPropertyCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            FontChooser chooser = d as FontChooser;
            if (e.NewValue == null)
                return;
            if ((bool)e.NewValue == true)
                chooser.lstFamily.ItemTemplate = chooser.Resources["fontFamilyData"] as DataTemplate;
            else
                chooser.lstFamily.ItemTemplate = chooser.Resources["fontFamilyDataWithoutPreview"] as DataTemplate;
        }

    }
}
