﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace WpfNotepad
{
	class MyTextBox : TextBox
	{
		static readonly DependencyProperty DP_SelectionStart = 
			DependencyProperty.Register(
				"BindableSelectionStart",
				typeof(int),
				typeof(MyTextBox),
				new PropertyMetadata(OnBindableSelectionStartChanged));

		static readonly DependencyProperty DP_SelectionLength =
			DependencyProperty.Register(
				"BindableSelectionLength",
				typeof(int),
				typeof(MyTextBox),
				new PropertyMetadata(OnBindableSelectionLengthChanged));

		public int BindableSelectionStart
		{
			get
			{
				return (int)this.GetValue(DP_SelectionStart);
			}

			set
			{
				this.SetValue(DP_SelectionStart, value);
			}
		}

		public int BindableSelectionLength
		{
			get
			{
				return (int)this.GetValue(DP_SelectionLength);
			}

			set
			{
				this.SetValue(DP_SelectionLength, value);
			}
		}

		public MyTextBox() : base()
		{
			this.SelectionChanged += MyTextBox_SelectionChanged;
		}

		private void MyTextBox_SelectionChanged(object sender, RoutedEventArgs e)
		{
			SetValue(DP_SelectionStart, SelectionStart);
			SetValue(DP_SelectionLength, SelectionLength);
		}

		private static void OnBindableSelectionStartChanged(
			DependencyObject dependencyObject,
			DependencyPropertyChangedEventArgs args)
		{
			var self = dependencyObject as MyTextBox;
		}

		private static void OnBindableSelectionLengthChanged(
			DependencyObject dependencyObject,
			DependencyPropertyChangedEventArgs args)
		{
			var self = dependencyObject as MyTextBox;
		}

	}
}
