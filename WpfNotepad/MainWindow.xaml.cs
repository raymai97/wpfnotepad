﻿using System;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using WpfFontDialog;
using TaskDialogInterop;
using Microsoft.Win32;
using System.IO;
using System.Windows.Interop;

namespace WpfNotepad
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		string currFilePath = null;
		Encoding currFileEncoding = new UTF8Encoding(false);

		public MainWindow()
		{
			InitializeComponent();
			RenderOptions.ProcessRenderMode = RenderMode.SoftwareOnly;
		}
		
		ResourceDictionary AppResources
		{
			get
			{
				return Application.Current.Resources;
			}
		}

		bool ConfirmCanDiscardCurrentFile
		{
			get
			{
				if (!CurrFileChanged) return true;
				var tdo = new TaskDialogOptions();
				tdo.AllowDialogCancellation = true;
				tdo.Owner = this;
				tdo.Title = AppResources["appName"].ToString();
				tdo.MainInstruction =
					AppResources["ask_want_save_this_file"].ToString() +
					Environment.NewLine;
				if (CurrFilePath != null)
				{
					tdo.MainInstruction += CurrFileName +
						Environment.NewLine;
				}
				tdo.CustomButtons = new string[] {
					AppResources["save"].ToString() + " (&Y)",
					AppResources["dont_save"].ToString() + " (&N)",
					AppResources["cancel"].ToString() + " (&C)"
				};
				var ret = TaskDialog.Show(tdo)
					.CustomButtonResult.GetValueOrDefault(2);
				if (ret == 0 && !SaveFile()) { return false; }
				if (ret == 2) { return false; }
				return true;
			}
		}

		public bool CurrFileChanged
		{
			get;
			private set;
		}

		public string CurrFilePath
		{
			get
			{
				return currFilePath;
			}
			private set
			{
				currFilePath = value;
				UpdateWindowTitle();
			}
		}

		public string CurrFileName
		{
			get
			{
				return System.IO.Path.GetFileName(currFilePath);
			}
		}

		public bool WordWrap
		{
			get
			{
				return txtEdit.HorizontalScrollBarVisibility ==
					ScrollBarVisibility.Disabled;
			}
			private set
			{
				bool was = WordWrap;
				txtEdit.HorizontalScrollBarVisibility =
					was ? ScrollBarVisibility.Visible :
					ScrollBarVisibility.Disabled;
				txtEdit.TextWrapping =
					was ? TextWrapping.NoWrap : TextWrapping.Wrap;
				ScrollToCaret(txtEdit);
				miWordWrap.IsChecked = WordWrap;
			}
		}

		void ScrollToCaret(TextBox txt)
		{
			// Notify textbox to scroll as it should
			txt.CaretIndex = txt.CaretIndex;
		}

		void UpdateWindowTitle()
		{
			var appName = AppResources["appName"].ToString();
			Title = currFilePath != null ?
				$"{CurrFileName} - {appName}" : appName;
		}

		void UpdateLnCh()
		{
			int iCaret = txtEdit.CaretIndex;
			int iRow = txtEdit.GetLineIndexFromCharacterIndex(iCaret);
			int iRowFirstChar = txtEdit.GetCharacterIndexFromLineIndex(iRow);
			int iCol = iCaret - iRowFirstChar;

			tbkLn.Text = $"{iRow + 1}";
			tbkCh.Text = $"{iCol + 1}";

			//tbkStatus.Text =
			//	txtEdit.SelectionLength > 0 ?
			//	AppResources["selected_char_count"].ToString() +
			//	txtEdit.SelectionLength : "";
		}

		string GetOpenFilePath()
		{
			var ofd = new OpenFileDialog();
			ofd.Filter = AppResources["fileDlg_filter"].ToString();
			if (ofd.ShowDialog().GetValueOrDefault(false))
			{
				return ofd.FileName;
			}
			return null;
		}

		string GetSaveFilePath()
		{
			var sfd = new SaveFileDialog();
			sfd.Filter = AppResources["fileDlg_filter"].ToString();
			if (sfd.ShowDialog().GetValueOrDefault(false))
			{
				return sfd.FileName;
			}
			return null;
		}

		bool NewFile()
		{
			if (ConfirmCanDiscardCurrentFile)
			{
				txtEdit.Clear();
				CurrFilePath = null;
				CurrFileChanged = false;
				return true;
			}
			return false;
		}

		bool OpenFile()
		{
			if (ConfirmCanDiscardCurrentFile)
			{
				var filePath = GetOpenFilePath();
				if (filePath != null)
				{
					MyOpenFile(filePath);
					return true;
				}
			}
			return false;
		}

		bool SaveFile()
		{
			if (currFilePath != null)
			{
				MySaveFile(currFilePath);
				return true;
			}
			else
			{
				return SaveFileAs();
			}
		}

		bool SaveFileAs()
		{
			var filePath = GetSaveFilePath();
			if (filePath != null)
			{
				MySaveFile(filePath);
				return true;
			}
			return false;
		}

		bool Exit()
		{
			if (ConfirmCanDiscardCurrentFile)
			{
				Application.Current.Shutdown(0);
				return true;
			}
			return false;
		}

		void ToggleWordWrap()
		{
			WordWrap = !WordWrap;
		}

		void ChangeFont()
		{
			var dlg = new FontDialog(false);
			dlg.Owner = this;
			dlg.Font = FontInfo.GetControlFont(txtEdit);
			if (dlg.ShowDialog() == true)
			{
				FontInfo.ApplyFont(txtEdit, dlg.Font);
			}
		}

		void MyOpenFile(string filePath)
		{
			try
			{
				using (var sr = new StreamReader(
					filePath, currFileEncoding))
				{
					txtEdit.Text = sr.ReadToEnd();
				}
				CurrFilePath = filePath;
				CurrFileChanged = false;
			}
			catch (Exception ex)
			{
				TaskDialog.ShowMessage(this,
					ex.Message,
					AppResources["error_occured"].ToString(),
					TaskDialogCommonButtons.Close,
					VistaTaskDialogIcon.Error);
			}
		}

		void MySaveFile(string filePath)
		{
			try
			{
				using (var sw = new StreamWriter(
					filePath, false, currFileEncoding))
				{
					sw.Write(txtEdit.Text);
				}
				CurrFilePath = filePath;
				CurrFileChanged = false;
			}
			catch (Exception ex)
			{
				TaskDialog.ShowMessage(this,
					ex.Message,
					AppResources["error_occured"].ToString(),
					TaskDialogCommonButtons.Close,
					VistaTaskDialogIcon.Error);
			}
		}

		private void Window_Loaded(object sender, RoutedEventArgs e)
		{
			txtEdit.Focus();
			UpdateLnCh();
			var args = Environment.GetCommandLineArgs();
			if (args.Length > 1)
			{
				MyOpenFile(args[1]);
			}
		}

		private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (!ConfirmCanDiscardCurrentFile)
			{
				e.Cancel = true;
			}
		}

		private void txtEdit_TextChanged(object sender, TextChangedEventArgs e)
		{
			CurrFileChanged = true;
		}

		private void cmdNewFile(object sender, ExecutedRoutedEventArgs e)
		{
			NewFile();
		}

		private void cmdOpenFile(object sender, ExecutedRoutedEventArgs e)
		{
			OpenFile();
		}

		private void cmdSaveFile(object sender, ExecutedRoutedEventArgs e)
		{
			SaveFile();
		}

		private void cmdSaveFileAs(object sender, ExecutedRoutedEventArgs e)
		{
			SaveFileAs();
		}

		private void cmdExit(object sender, ExecutedRoutedEventArgs e)
		{
			Exit();
		}

		private void cmdToggleWordWrap(object sender, ExecutedRoutedEventArgs e)
		{
			ToggleWordWrap();
		}

		private void cmdChangeFont(object sender, ExecutedRoutedEventArgs e)
		{
			ChangeFont();
		}

		private void txtEdit_SelectionChanged(object sender, RoutedEventArgs e)
		{
			UpdateLnCh();
		}

		private void cmdSetUiLang_en_us(object sender, ExecutedRoutedEventArgs e)
		{
			App.UILanguageCode = "en-us";
		}

		private void cmdSetUiLang_ja_jp(object sender, ExecutedRoutedEventArgs e)
		{
			App.UILanguageCode = "ja-jp";
		}

	}
}
