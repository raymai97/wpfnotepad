﻿using System;
using System.Collections.Generic;
using System.Windows.Media;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace WpfNotepad
{
	class FontDialog
	{
		public FontFamily FontFamily
		{
			get; set;
		}

		public double FontSize
		{
			get; set;
		}

		public FontStretch FontStretch
		{
			get; set;
		}

		public FontStyle FontStyle
		{
			get; set;
		}

		public FontWeight FontWeight
		{
			get; set;
		}

		public bool ShowDialog()
		{
			var dlg = new System.Windows.Forms.FontDialog();

			return false;
		}

		System.Drawing.Font ToWinformFont()
		{
			string familyName = "";
			foreach (var name in FontFamily.FamilyNames.Values)
			{
				familyName = name;
			}
			float fontSize = (float)FontSize;
			var fontStyle = System.Drawing.FontStyle.Regular;
			var x = FontStretch;
			
				
		}

	}
}
